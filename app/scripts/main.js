$(document).ready(function() {
  $('.ao-home-carousel').slick({
    dots: false,
    arrows: true,
    infinite: true,
    autoplay: true,
    speed: 500
  });

  $('.ao-carousel-goods').slick({
    dots: false,
    arrows: true,
    infinite: true,
    autoplay: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 2,
    responsive: [{
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

  $('.ao-header__search-droper').click(function() {
    $('.ao-search').slideToggle('down');
  });

  $('.ao-search__close').click(function() {
    $('.ao-search').slideToggle('down');
  });

  $('.ao-mob-header__map-droper').click(function() {
    $(this).next().slideToggle('down');
  });

  $('.ao-mob-header__phone-droper').click(function() {
    $(this).next().slideToggle('down');
  });

  $('.ao-mob-header__droper').click(function() {
    $(this).next().slideToggle('down');
  });

  $('.ao-mob-header__catalog-droper').click(function() {
    $(this).next().slideToggle('down');
  });

  $('.ao-header__basket-droper').click(function() {
    $(this).next().slideToggle('down');
  });

  $('.ao-mob-header__basket-droper').click(function() {
    $(this).next().slideToggle('down');
  });

});